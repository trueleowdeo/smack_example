package ug.student.smackexample;

import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements ConnectionListener {
    Timer timer=null;
    int timetaken=0;
    TextView tvTimeTakenBySmack=null;
    TextView tvConnectionState=null;
    TextView tvSmackErrors=null;
    XMPPTCPConnection connection=null;
    static Thread thesmackthread=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTimeTakenBySmack=findViewById(R.id.textview_smacktime);
        tvConnectionState=findViewById(R.id.textview_smackconnectionstate);
        tvSmackErrors=findViewById(R.id.textview_smackerrors);
        timer=new Timer();
        if(thesmackthread==null)
        {
            thesmackthread=new Thread(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();

                    try
                    {
                        XMPPTCPConnectionConfiguration cc= XMPPTCPConnectionConfiguration.builder()
                                .setCompressionEnabled(true)
                                .setUsernameAndPassword("smackuser","ilovesmack")
                                .setXmppDomain("xmpp.jp")
                                .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                                .build();
                        connection=new XMPPTCPConnection(cc);
                        connection.addConnectionListener(MainActivity.this);

                        timer.scheduleAtFixedRate(new TimerTask() {
                            @Override
                            public void run() {
                                tvTimeTakenBySmack.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        tvTimeTakenBySmack.setText("Time taken by Smack To Connect : "+timetaken+" Seconds");
                                    }
                                });
                                timetaken+=1;
                            }
                        },new Date(),1000);

                        connection.connect();
                        connection.login();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        timer.cancel();
                        final Exception fe=e;
                        tvSmackErrors.post(new Runnable() {
                            @Override
                            public void run() {
                                tvSmackErrors.setText("Connection exception "+fe.getMessage());
                            }
                        });
                    }
                    Looper.loop();
                }
            });
            thesmackthread.start();
        }

    }

    @Override
    public void connected(XMPPConnection connection) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionState.setText("Connected!!");
                timer.cancel();
                Toast.makeText(MainActivity.this,"connected!!",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionState.setText("Authenticated!!");
            }
        });
    }

    @Override
    public void connectionClosed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionState.setText("Connection Closed");
            }
        });
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        final Exception fe=e;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvSmackErrors.setText("Connection exception "+fe.getMessage());
            }
        });
    }
}
